﻿$engine JScript
$uname extdiff
$dname Расширенное сравнение объектов.
$addin stdlib
$addin stdcommands

// (c) Сосна Евгений <shenja@sosna.zp.ua>

stdlib.require('TextWindow.js', SelfScript);
stdlib.require('log4js.js', SelfScript);
spell = stdlib.require(stdlib.getSnegopatMainFolder() + 'scripts\\SpellChecker.js');

SelfScript.self['macrosНаше любое действие'] = function() {
    extDiff();
}

SelfScript.self['macrosПроверка синтаксиса плюс орфаграфии текущего модуля'] = function() {
    syntaxAndSpellCheck();
}

function extDiff(){
    Message("extDiff");
}

function syntaxAndSpellCheck() {
    if(stdcommands.Frntend.SyntaxCheck.getState().enabled)
        stdcommands.Frntend.SyntaxCheck.send();
    
    var wnd = GetTextWindow();
    
    var text = "";
    if (wnd){
        text = wnd.GetText();
        text = text.split('\n');
        spellChecker = spell.GetSpellChecker();
    } else {
        return;
    }

    var sciMgr = addins.byUniqueName("SciColorerV8Manager").object;
    var hwnd = sciMgr.getActiveScintillaHandle();
    if (hwnd){
        var nextModLine = sciMgr.SendSciMessage(hwnd,sciMgr.SCI_GETNEXTMODLINE,0,-1);
        var curModLine = -1;
        while (nextModLine > curModLine){
             while (sciMgr.SendSciMessage(hwnd,sciMgr.SCI_GETMODLINESTATE,nextModLine) > 0){
                spellChecker.SpellLine(text[nextModLine-1], wnd, nextModLine-1);
                nextModLine++;
            }
            curModLine = nextModLine;
            nextModLine = sciMgr.SendSciMessage(hwnd,sciMgr.SCI_GETNEXTMODLINE,0,curModLine);
        }   
   }
}

